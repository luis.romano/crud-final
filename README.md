# CRUD APP

Aplicacion crud desarrollada como trabajo final de PP1

## Instalación

`git clone https://gitlab.com/luis.romano/crud-final `

`cd crud-final `

`docker-compose up -d`

## Creditos

Luis Romano

luis.romano@itscordoba.edu.ar
